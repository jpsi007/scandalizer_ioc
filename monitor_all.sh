#!/bin/bash

camonitor                     \
  hcYield:test1:Count         \
  hcYield:test1:TotalCount    \
  hcYield:test1:TotalCharge   \
  hcYield:test1:TotalTime     \
  hcYield:test1:Yield         \
  hcYield:test1:TotalYield    \
  hcYield:test1:Charge        \
  hcYield:test1:Time          \

#  hcSHMS:DC:Mult              \
#  hcSHMS:DC:Occupancy         \
#  hcSHMS:TrackingEff          \
#  hcSHMS:TrackingEff:Unc      \
#  hcSHMS:Hod:Mult             \
#  hcSHMS:incl:e:yields        \
#  hcSHMS:incl:pion:yields     \
#  hcHMS:DC:Mult               \
#  hcHMS:DC:Occupancy          \
#  hcHMS:TrackingEff           \
#  hcHMS:TrackingEff:Unc       \
#  hcHMS:Hod:Mult              \
#  hcHMS:incl:e:yields         \
#  hcHMS:incl:pion:yields      \
#

