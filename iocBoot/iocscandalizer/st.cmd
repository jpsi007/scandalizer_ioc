#!../../bin/linux-x86_64/scandalizer

#- You may have to change scandalizer to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/scandalizer.dbd"
scandalizer_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadTemplate "db/user.substitutions"

dbLoadRecords "db/spec_data_monitor.db", "spec=HMS"
dbLoadRecords "db/spec_data_monitor.db", "spec=SHMS"

dbLoadRecords "db/yield_monitor.db", "count=test1"

#dbLoadRecords "db/scandalizerVersion.db", "user=whit"
#dbLoadRecords "db/dbSubExample.db", "user=whit"

#- Set this to see messages from mySub
#var mySubDebug 1

#- Run this to trace the stages of iocInit
#traceIocInit

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncExample, "user=whit"
